<div align="center">
  <h1>Scribe</h1>
</div>

<div align="center">
  <small>Version: <b>0.1.0</b></small>
</div>

<div align="center">
  <b>Single Sign On in Rust</b>
</div>

<div align="center">
  <img src="https://img.shields.io/badge/made%20with-rust-orange.svg?style=flat-square" alt="Made With Rust" />
  <a href="https://github.com/seanmonstar/warp">
    <img src="https://img.shields.io/badge/built%20with-warp-red.svg?style=flat-square" alt="Built With Warp" />
  </a>
</div>
<div align="center">
  <a href="https://ci.appveyor.com/project/Txuritan/scribe">
    <img src="https://img.shields.io/appveyor/ci/Txuritan/scribe.svg?style=flat-square" alt="Appveyor Build Status" />
  </a>
  <a href="https://travis-ci.org/storyarchive/scribe">
    <img src="https://img.shields.io/travis/storyarchive/scribe.svg?style=flat-square" alt="Travis Build Status" />
  </a>
</div>

<div align="center">
  <a href="https://github.com/storyarchive/scribe/blob/master/LICENSE-APACHE">
    <img src="https://img.shields.io/badge/license-Apache-blue.svg?style=flat-square" alt="License Apache 2.0" />
  </a>
  <a href="https://github.com/storyarchive/scribe/blob/master/LICENSE-MIT">
    <img src="https://img.shields.io/badge/license-MIT-green.svg?style=flat-square" alt="License Mit" />
  </a>
</div>

---

A SSO(Single Sign On) for Archive, Codex, Manuscript, Scroll, and Tablet.