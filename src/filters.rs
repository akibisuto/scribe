pub(self) use {
    crate::{handlers, PostgresPool},
    std::convert::Infallible,
    warp::{filters, path, Filter, Rejection, Reply},
};

pub(self) fn with_state(
    pool: PostgresPool,
) -> impl Filter<Extract = (PostgresPool,), Error = Infallible> + Clone {
    warp::any().map(move || pool.clone())
}

pub mod get {
    use super::*;

    pub fn register(
        pool: PostgresPool,
    ) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
        path("register")
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get::register)
    }

    pub fn sign_in(
        pool: PostgresPool,
    ) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
        path("sign-in")
            .and(warp::get())
            .and(with_state(pool))
            .and_then(handlers::get::sign_in)
    }

    pub fn logout(
        pool: PostgresPool,
    ) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
        path("logout")
            .and(warp::get())
            .and(filters::cookie::optional("scribe-token"))
            .and(with_state(pool))
            .and_then(handlers::get::logout)
    }
}

pub mod post {
    use {
        super::*,
        crate::models::{RegisterForm, SignInForm},
    };

    fn with_form<T: serde::de::DeserializeOwned + Send>(
    ) -> impl Filter<Extract = (T,), Error = Rejection> + Clone {
        warp::body::content_length_limit(1024 * 32).and(warp::body::form())
    }

    pub fn register(
        pool: PostgresPool,
    ) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
        path("register")
            .and(warp::post())
            .and(with_form::<RegisterForm>())
            .and(with_state(pool))
            .and_then(handlers::post::register)
    }

    pub fn sign_in(
        pool: PostgresPool,
    ) -> impl Filter<Extract = impl Reply, Error = Rejection> + Clone {
        path("sign-in")
            .and(warp::post())
            .and(with_form::<SignInForm>())
            .and(with_state(pool))
            .and_then(handlers::post::sign_in)
    }
}
