use {
    crate::{PostgresPool, pages::{RENDER_CACHE, Templates}},
    warp::{Rejection, Reply, reply},
};

pub async fn register(pool: PostgresPool) -> Result<impl Reply, Rejection> {
    let _conn = pool.get().await;

    Ok(reply::html(RENDER_CACHE[Templates::Register].clone()))
}

pub async fn sign_in(pool: PostgresPool) -> Result<impl Reply, Rejection> {
    let _conn = pool.get().await;

    Ok(reply::html(RENDER_CACHE[Templates::SignIn].clone()))
}

pub async fn logout(token: Option<String>, pool: PostgresPool) -> Result<impl Reply, Rejection> {
    let _conn = pool.get().await;

    Ok("hello")
}
