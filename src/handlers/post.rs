use {
    crate::{
        models::{RegisterForm, SignInForm},
        PostgresPool,
    },
    warp::{Rejection, Reply},
};

pub async fn register(form: RegisterForm, pool: PostgresPool) -> Result<impl Reply, Rejection> {
    let _conn = pool.get().await;

    Ok("hello")
}

pub async fn sign_in(form: SignInForm, pool: PostgresPool) -> Result<impl Reply, Rejection> {
    let _conn = pool.get().await;

    Ok("hello")
}
