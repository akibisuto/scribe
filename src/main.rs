mod handlers;
mod pages;

mod config;
mod filters;
mod models;

use {
    crate::filters::*, bb8::Pool, bb8_postgres::PostgresConnectionManager, tokio_postgres::NoTls,
    warp::Filter,
};

pub(crate) type PostgresPool = Pool<PostgresConnectionManager<NoTls>>;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(tracing::Level::DEBUG)
        // .json()
        .finish();

    tracing::subscriber::set_global_default(subscriber)?;

    tracing_log::LogTracer::init()?;

    let pool = Pool::builder()
        .build(PostgresConnectionManager::new(
            {
                let mut config = tokio_postgres::Config::new();

                config.application_name("akibisuto-scribe");

                config
                    .dbname("akibisuto-scribe")
                    .host("localhost")
                    .port(5432)
                    .user("akibisuto-scribe")
                    .password("scribe");

                config
            },
            NoTls,
        ))
        .await?;

    tracing::debug!("Postgres connection test: {}", select(&pool).await?);

    tracing::debug!("Initializing template render cache");
    once_cell::sync::Lazy::force(&pages::RENDER_CACHE);

    let scribe = get::register(pool.clone())
        .or(post::register(pool.clone()))
        .or(get::sign_in(pool.clone()))
        .or(post::sign_in(pool.clone()))
        .or(get::logout(pool.clone()))
        .with(warp::log("scribe"));

    let (addr, server) =
        warp::serve(scribe).bind_with_graceful_shutdown(([0, 0, 0, 0], 3030), async {
            tokio::signal::ctrl_c()
                .await
                .expect("failed to install CTRL+C signal handler")
        });

    tracing::info!("Started Scribe v{} on {}", env!("CARGO_PKG_VERSION"), addr);

    server.await;

    Ok(())
}

async fn select(pool: &PostgresPool) -> anyhow::Result<i32> {
    let connection = pool.get().await?;

    let stmt = connection.prepare("SELECT 1").await?;

    let row = connection.query_one(&stmt, &[]).await?;

    Ok(row.get::<usize, i32>(0))
}
