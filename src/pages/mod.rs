pub(crate) mod register;
pub(crate) mod sign_in;

use {once_cell::sync::Lazy, enum_map::EnumMap};

pub(crate) use crate::pages::{register::Register, sign_in::SignIn};

pub(crate) static RENDER_CACHE: Lazy<EnumMap<Templates, String>> = Lazy::new(|| {
    use askama::Template;

    enum_map::enum_map! {
        Templates::Register => Register::default().render().unwrap(),
        Templates::SignIn => SignIn::default().render().unwrap(),
    }
});

#[derive(enum_map::Enum)]
pub(crate) enum Templates {
    Register,
    SignIn,
}
