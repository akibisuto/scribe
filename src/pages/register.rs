#[derive(askama::Template)]
#[template(path = "register.html")]
pub(crate) struct Register {}

impl Default for Register {
    fn default() -> Self {
        Self {}
    }
}
