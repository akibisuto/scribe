#[derive(askama::Template)]
#[template(path = "sign-in.html")]
pub(crate) struct SignIn {}

impl Default for SignIn {
    fn default() -> Self {
        Self {}
    }
}
